# Erika s3004 Computer Interface

![Build status](https://ci.jbb.ghsq.ga/api/badges/jbb/erika_S3004/status.svg)

Tested on a SIGMA SM 8200i, which is supposedly the same hardware just under the brand name used in Western Germany.

This project is based on information from [Chaostreff Potsdam](https://github.com/Chaostreff-Potsdam/erika3004).

## Hardware
- A 5V USB TTL adapter (I used [this](https://www.amazon.de/USB-TTL-Konverter-Modul-mit-eingebautem-CP2102/dp/B00AFRXKFU) one, which I still had lying around)
- A few pieces of wire, to connect the pins of the TTL USB Adapter with the typewriter.

Note: although the typewriter turns on when the 5V pin is connected, the external power source is still needed in order to do anything with it.

| USB TTL | Erika |
|---------|-------|
| RX      | TX    |
| TX      | RX    |
| 5V      | 5V    |
| CTS     | RTS   |
| GND     | GND   |
| RTS     | DTD   |

![Connectors](doc/connectors.svg)


![Cable wiring](doc/interface.JPG)

## Software

### Building and running

Building:
```
cargo build --release
```

Printing some text:
```
./target/release/erika-cli print-file text.txt
```

For more commands, have a look at the help page:
```
./target/release/erika-cli --help
```

### CUPS Integration

The Erika s3004 and derived typewriters can be used with cups support for dot matrix printers.

To set this up, first install this project: `make`, `sudo make install`. For pdf support, you need to have the `pdftotext` util from `poppler-utils` insalled.

Then, visit the cups web interface in your webbrowser. It is located on http://localhost:631, and add a new "AppSocket/HP JetDirect" printer.
The web interface now asks for a URI. Enter `serial:/dev/ttyUSB0`. If your serial adapter is not on `/dev/ttyUSB0` (you can test with erika-cli), replace it with the proper path.

On the next page, choose "RTS/CTS (Hardware)" as flow control.

Next, enter the name and description for the new printer.

When you are asked to choose a vendor or PPD file, select the ppd file from this repository. You can find it in `cups/ppd/ErikaS3004.ppd`
